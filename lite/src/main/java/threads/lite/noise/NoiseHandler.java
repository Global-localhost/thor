package threads.lite.noise;

import java.nio.ByteBuffer;
import java.util.Objects;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.PeerId;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Server;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.host.LiteStream;
import threads.lite.mplex.MuxedTransport;
import threads.lite.utils.DataHandler;

public class NoiseHandler implements ProtocolHandler {
    private static final String TAG = NoiseHandler.class.getSimpleName();

    private final Server server;

    public NoiseHandler(Server server) {
        this.server = server;
    }


    public Noise.NoiseState getResponder(Stream stream) throws Exception {
        Noise.NoiseState state = (Noise.NoiseState) stream.getAttribute(StreamHandler.NOISE);
        PeerId peerId = Objects.requireNonNull((PeerId) stream.getAttribute(StreamHandler.PEER));

        if (state == null) {
            state = Noise.getResponder(peerId, server.getKeys());
            stream.setAttribute(StreamHandler.NOISE, state);
        }
        return state;
    }

    @Override
    public String getProtocol() {
        return IPFS.NOISE_PROTOCOL;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.NOISE_PROTOCOL));
        LiteStream liteStream = Objects.requireNonNull((LiteStream) stream);
        stream.setAttribute(StreamHandler.TRANSPORT, new Handshake(liteStream.getQuicStream()));
        LogUtils.info(TAG, "Transport set to Handshake");
    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {

        Noise.Response response = getResponder(stream).handshake(data.array());

        byte[] msg = response.getMessage();
        if (msg != null) {
            stream.writeOutput(Noise.encodeNoiseMessage(msg));
        }


        CipherStatePair cipherStatePair = response.getCipherStatePair();
        if (cipherStatePair != null) {
            LiteStream liteStream = Objects.requireNonNull((LiteStream) stream);

            // upgrade connection
            MuxedTransport muxedTransport = new MuxedTransport(liteStream.getQuicStream(),
                    cipherStatePair.getSender(),
                    cipherStatePair.getReceiver());
            stream.setAttribute(StreamHandler.TRANSPORT, muxedTransport);

            LogUtils.error(TAG, "Transport set to MuxedTransport, do hole punch initiate");
        }

    }
}
