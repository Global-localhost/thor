package threads.lite.core;

import androidx.annotation.NonNull;

import java.util.List;

import threads.lite.cid.Multiaddr;

public interface SwarmStore {
    void storeMultiaddr(@NonNull Multiaddr multiaddr);

    void removeMultiaddr(@NonNull Multiaddr multiaddr);

    List<Multiaddr> getMultiaddrs();
}
