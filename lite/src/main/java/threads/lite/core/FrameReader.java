package threads.lite.core;

import java.nio.ByteBuffer;

public interface FrameReader {

    ByteBuffer getFrame(ByteBuffer data) throws Exception;
}
