/*
 * Copyright 2015-2022 the original author or authors
 *
 * This software is licensed under the Apache License, Version 2.0,
 * the GNU Lesser General Public License version 2 or later ("LGPL")
 * and the WTFPL.
 * You may choose either license to govern your use of this software only
 * upon the condition that you accept all of the terms of either
 * the Apache License 2.0, the LGPL 2.1+ or the WTFPL.
 */
package threads.lite.minidns;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

public final class DnsDataSource {

    private final int udpPayloadSize = 1024;
    /**
     * DNS timeout.
     */
    private final int timeout = 5000;
    private final QueryMode queryMode = QueryMode.dontCare;


    public int getUdpPayloadSize() {
        return udpPayloadSize;
    }

    public QueryMode getQueryMode() {
        return queryMode;
    }

    public DnsQueryResult query(DnsMessage message, InetAddress address, int port) throws IOException {
        final DnsDataSource.QueryMode queryMode = getQueryMode();
        boolean doUdpFirst;
        switch (queryMode) {
            case dontCare:
            case udpTcp:
                doUdpFirst = true;
                break;
            case tcp:
                doUdpFirst = false;
                break;
            default:
                throw new IllegalStateException("Unsupported query mode: " + queryMode);
        }

        List<IOException> ioExceptions = new ArrayList<>(2);
        DnsMessage dnsMessage = null;

        if (doUdpFirst) {
            try {
                dnsMessage = queryUdp(message, address, port);
            } catch (IOException e) {
                ioExceptions.add(e);
            }
        }

        try {
            dnsMessage = queryTcp(message, address, port);
        } catch (IOException e) {
            ioExceptions.add(e);
            MultipleIoException.throwIfRequired(ioExceptions);
        }

        return new DnsQueryResult(dnsMessage);
    }

    private DnsMessage queryUdp(DnsMessage query, InetAddress address, int port) throws IOException {
        DatagramPacket packet = query.asDatagram(address, port);
        byte[] buffer = new byte[udpPayloadSize];
        try (DatagramSocket socket = createDatagramSocket()) {
            socket.setSoTimeout(timeout);
            socket.send(packet);
            packet = new DatagramPacket(buffer, buffer.length);
            socket.receive(packet);
            DnsMessage dnsMessage = new DnsMessage(packet.getData());
            if (dnsMessage.id != query.id) {
                throw new DnsException.IdMismatch(query, dnsMessage);
            }
            return dnsMessage;
        }
    }

    private DnsMessage queryTcp(DnsMessage message, InetAddress address, int port) throws IOException {
        try (Socket socket = createSocket()) {
            SocketAddress socketAddress = new InetSocketAddress(address, port);
            socket.connect(socketAddress, timeout);
            socket.setSoTimeout(timeout);
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            message.writeTo(dos);
            dos.flush();
            DataInputStream dis = new DataInputStream(socket.getInputStream());
            int length = dis.readUnsignedShort();
            byte[] data = new byte[length];
            int read = 0;
            while (read < length) {
                read += dis.read(data, read, length - read);
            }
            DnsMessage dnsMessage = new DnsMessage(data);
            if (dnsMessage.id != message.id) {
                throw new DnsException.IdMismatch(message, dnsMessage);
            }
            return dnsMessage;
        }
    }

    /**
     * Create a {@link Socket} using the system default {@link javax.net.SocketFactory}.
     *
     * @return The new {@link Socket} instance
     */
    private Socket createSocket() {
        return new Socket();
    }

    /**
     * Create a {@link DatagramSocket} using the system defaults.
     *
     * @return The new {@link DatagramSocket} instance
     * @throws SocketException If creation of the {@link DatagramSocket} fails
     */
    private DatagramSocket createDatagramSocket() throws SocketException {
        return new DatagramSocket();
    }

    public enum QueryMode {
        /**
         * Perform the query mode that is assumed "best" for that particular case.
         */
        dontCare,

        /**
         * Try UDP first, and if the result is bigger than the maximum UDP payload size, or if something else goes wrong, fallback to TCP.
         */
        udpTcp,

        /**
         * Always use only TCP when querying DNS servers.
         */
        tcp,
    }

    public interface OnResponseCallback {
        void onResponse(DnsMessage request, DnsQueryResult result);
    }

}
