package threads.thor.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.Objects;

import threads.thor.core.tabs.TABS;
import threads.thor.core.tabs.Tab;
import threads.thor.core.tabs.TabsDatabase;

public class TabsModel extends AndroidViewModel {
    private final TabsDatabase tabsDatabase;
    @NonNull
    private final MutableLiveData<Long> currentTab = new MutableLiveData<>(0L);

    public TabsModel(@NonNull Application application) {
        super(application);
        tabsDatabase = TABS.getInstance(
                application.getApplicationContext()).getTabsDatabase();
    }

    public LiveData<List<Tab>> getTabs() {
        return tabsDatabase.tabDao().getLiveDataTabs();
    }

    @NonNull
    public MutableLiveData<Long> getCurrentTab() {
        return currentTab;
    }

    public void setCurrentTab(Long tab) {
        getCurrentTab().postValue(tab);
    }


    public long getCurrentTabValue() {
        Long value = getCurrentTab().getValue();
        Objects.requireNonNull(value);
        return value;
    }
}
