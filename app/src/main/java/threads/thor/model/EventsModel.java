package threads.thor.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import threads.thor.core.events.EVENTS;
import threads.thor.core.events.Event;
import threads.thor.core.events.EventsDatabase;

public class EventsModel extends AndroidViewModel {

    private final EventsDatabase eventsDatabase;

    public EventsModel(@NonNull Application application) {
        super(application);
        eventsDatabase = EVENTS.getInstance(
                application.getApplicationContext()).getEventsDatabase();
    }

    public LiveData<Event> uri() {
        return eventsDatabase.eventDao().getEvent(EVENTS.URI);
    }

    public LiveData<Event> fatal() {
        return eventsDatabase.eventDao().getEvent(EVENTS.FATAL);
    }

    public LiveData<Event> error() {
        return eventsDatabase.eventDao().getEvent(EVENTS.ERROR);
    }

    public LiveData<Event> warning() {
        return eventsDatabase.eventDao().getEvent(EVENTS.WARNING);
    }

    public LiveData<Event> info() {
        return eventsDatabase.eventDao().getEvent(EVENTS.INFO);
    }


    public LiveData<Event> offline() {
        return eventsDatabase.eventDao().getEvent(EVENTS.OFFLINE);
    }

    public LiveData<Event> online() {
        return eventsDatabase.eventDao().getEvent(EVENTS.ONLINE);
    }

    public void removeEvent(@NonNull final Event event) {
        eventsDatabase.eventDao().deleteEvent(event);
    }

    public LiveData<Event> permission() {
        return eventsDatabase.eventDao().getEvent(EVENTS.PERMISSION);
    }
}