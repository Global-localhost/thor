package threads.thor.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.sidesheet.SideSheetDialog;

import java.util.List;
import java.util.Objects;

import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.core.DOCS;
import threads.thor.core.tabs.TABS;
import threads.thor.core.tabs.Tab;
import threads.thor.model.TabsModel;
import threads.thor.utils.TabsAdapter;

public class TabsFragment extends DialogFragment {

    public static final String TAG = TabsFragment.class.getSimpleName();

    public static TabsFragment newInstance() {
        return new TabsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tabs, container, false);
        TabsModel tabsModel = new ViewModelProvider(requireActivity()).get(TabsModel.class);

        RecyclerView recyclerView = view.findViewById(R.id.card_recycler_view);
        Objects.requireNonNull(recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireContext());

        recyclerView.setLayoutManager(linearLayoutManager);

        TabsAdapter tabsAdapter = new TabsAdapter(new TabsAdapter.ViewHolderListener() {
            @Override
            public void onItemClicked(@NonNull Tab tab) {
                try {
                    tabsModel.setCurrentTab(tab.getIdx());

                    Objects.requireNonNull(getDialog()).dismiss();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }

            @Override
            public void onItemDismiss(@NonNull Tab tab) {

                try {
                    TABS tabs = TABS.getInstance(requireContext());
                    tabs.removeTab(tab);

                    if (tabsModel.getCurrentTabValue() == tab.getIdx()) {
                        List<Tab> tabList = tabs.getTabs();
                        if (tabList.isEmpty()) {
                            DOCS docs = DOCS.getInstance(requireContext());
                            tabsModel.setCurrentTab(docs.createHomepageTab());
                            Objects.requireNonNull(getDialog()).dismiss();
                        } else {
                            tabsModel.setCurrentTab(tabList.get(0).getIdx());
                        }
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }

            @Override
            public boolean isSelected(@NonNull Tab tab) {
                return tabsModel.getCurrentTabValue() == tab.getIdx();
            }
        });

        recyclerView.setAdapter(tabsAdapter);


        tabsModel.getTabs().observe(getViewLifecycleOwner(), tabs -> {
            if (tabs != null) {
                tabsAdapter.updateData(tabs);
                long idx = tabsModel.getCurrentTabValue();
                int position = getTabPosition(tabs, idx);
                linearLayoutManager.scrollToPosition(position);
            }
        });


        MaterialToolbar gridToolbar = view.findViewById(R.id.grid_toolbar);
        Objects.requireNonNull(gridToolbar);
        gridToolbar.setNavigationOnClickListener(v -> {
            try {
                DOCS docs = DOCS.getInstance(requireContext());
                tabsModel.setCurrentTab(docs.createHomepageTab());
                Objects.requireNonNull(getDialog()).dismiss();
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        gridToolbar.getMenu().findItem(R.id.close_tabs).setOnMenuItemClickListener(item -> {

            if (item.getItemId() == R.id.close_tabs) {
                TABS.getInstance(requireContext()).clear();
                try {
                    DOCS docs = DOCS.getInstance(requireContext());
                    tabsModel.setCurrentTab(docs.createHomepageTab());
                    Objects.requireNonNull(getDialog()).dismiss();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            }

            return false;
        });
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new SideSheetDialog(requireContext());
    }


    public int getTabPosition(@NonNull List<Tab> tabs, long tab) {
        for (int i = 0; i < tabs.size(); i++) {
            if (tab == tabs.get(i).getIdx()) {
                return i;
            }
        }
        return 0; // in case not available
    }
}
