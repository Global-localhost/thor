package threads.thor.fragments;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.PixelCopy;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewDatabase;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowMetricsCalculator;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.navigationrail.NavigationRailView;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.sidesheet.SideSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import threads.lite.core.Cancellable;
import threads.lite.core.Session;
import threads.magnet.magnet.MagnetUri;
import threads.magnet.magnet.MagnetUriParser;
import threads.thor.InitApplication;
import threads.thor.LogUtils;
import threads.thor.MainActivity;
import threads.thor.R;
import threads.thor.core.DOCS;
import threads.thor.core.Settings;
import threads.thor.core.books.BOOKS;
import threads.thor.core.books.Bookmark;
import threads.thor.core.events.EVENTS;
import threads.thor.model.EventsModel;
import threads.thor.services.MimeTypeService;
import threads.thor.utils.AdBlocker;
import threads.thor.utils.CustomWebChromeClient;
import threads.thor.utils.HistoryAdapter;
import threads.thor.work.DownloadContentWorker;
import threads.thor.work.DownloadFileWorker;
import threads.thor.work.DownloadMagnetWorker;

public class BrowserFragment extends Fragment {
    private static final String TAG = BrowserFragment.class.getSimpleName();
    private WebView webView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearProgressIndicator progressIndicator;
    private NavigationRailView navigationRailView;
    private long lastClickTime;
    private WindowSizeClass widthWindowSizeClass = WindowSizeClass.COMPACT;
    private ActionMode actionMode;

    private void switchDisplayModes() {
        if (widthWindowSizeClass == WindowSizeClass.EXPANDED) {
            navigationRailView.setVisibility(View.VISIBLE);
        } else {
            navigationRailView.setVisibility(View.GONE);
        }
    }

    private void computeWindowSizeClasses() {
        WindowMetrics metrics = WindowMetricsCalculator.getOrCreate()
                .computeCurrentWindowMetrics(requireActivity());

        float widthDp = metrics.getBounds().width() /
                getResources().getDisplayMetrics().density;


        if (widthDp < 600f) {
            widthWindowSizeClass = WindowSizeClass.COMPACT;
        } else if (widthDp < 840f) {
            widthWindowSizeClass = WindowSizeClass.MEDIUM;
        } else {
            widthWindowSizeClass = WindowSizeClass.EXPANDED;
        }

    }

    public void invalidateOptionsMenu() {
        navigationRailView.getMenu().
                findItem(R.id.action_previous_page)
                .setEnabled(webView.canGoBack());

        navigationRailView.getMenu().
                findItem(R.id.action_next_page)
                .setEnabled(webView.canGoForward());

        navigationRailView.getMenu().
                findItem(R.id.action_download)
                .setVisible(downloadActive());
    }

    public void bookmark() {
        try {
            BOOKS books = BOOKS.getInstance(requireContext());
            String url = webView.getUrl();
            Bookmark bookmark = books.getBookmark(url);
            if (bookmark != null) {

                String msg = bookmark.getTitle();
                books.removeBookmark(bookmark);

                if (msg.isEmpty()) {
                    msg = url;
                }
                EVENTS.getInstance(requireContext()).warning(
                        getString(R.string.bookmark_removed, msg));
            } else {
                Bitmap bitmap = webView.getFavicon();
                String title = webView.getTitle();

                if (title == null) {
                    title = Uri.parse(url).getHost();
                }

                bookmark = books.createBookmark(url, title);
                if (bitmap != null) {
                    bookmark.setBitmapIcon(bitmap);
                }

                books.storeBookmark(bookmark);

                String msg = title;
                if (msg.isEmpty()) {
                    msg = url;
                }

                EVENTS events = EVENTS.getInstance(requireContext());
                events.warning(getString(R.string.bookmark_added, msg));

                requireActivity().invalidateOptionsMenu();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        try {
            computeWindowSizeClasses();
            switchDisplayModes();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_browser, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        computeWindowSizeClasses();

        progressIndicator = view.findViewById(R.id.progress_bar);
        progressIndicator.setVisibility(View.GONE);
        webView = view.findViewById(R.id.web_view);
        swipeRefreshLayout = view.findViewById(R.id.swipe_container);

        MaterialTextView offline_mode = view.findViewById(R.id.offline_mode);


        InitApplication.setWebSettings(webView, Settings.isJavascriptEnabled(requireContext()));

        CookieManager.getInstance().setAcceptThirdPartyCookies(webView, false);


        swipeRefreshLayout.setOnRefreshListener(() -> {
            try {
                swipeRefreshLayout.setRefreshing(true);
                reloadPage();
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            } finally {
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        navigationRailView = view.findViewById(R.id.navigation_rail);
        switchDisplayModes();

        navigationRailView.setOnItemSelectedListener(item -> {
            int id = item.getItemId();
            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            if (id == R.id.action_previous_page) {
                previousPage();
                return true;
            } else if (id == R.id.action_reload_page) {
                reloadPage();
                return true;
            } else if (id == R.id.action_next_page) {
                nextPage();
                return true;
            } else if (id == R.id.action_find_page) {
                findPage();
                return true;
            } else if (id == R.id.action_share) {
                share();
                return true;
            } else if (id == R.id.action_information) {
                info();
                return true;
            } else if (id == R.id.action_download) {
                download();
                return true;
            }
            return false;
        });

        CustomWebChromeClient mCustomWebChromeClient = new CustomWebChromeClient(requireActivity());
        webView.setWebChromeClient(mCustomWebChromeClient);


        webView.setDownloadListener((url, userAgent, contentDisposition, mimeType, contentLength) -> {

            try {

                String filename = URLUtil.guessFileName(url, contentDisposition, mimeType);
                Uri uri = Uri.parse(url);

                if (Objects.equals(uri.getScheme(), Settings.IPFS) ||
                        Objects.equals(uri.getScheme(), Settings.IPNS)) {
                    String res = uri.getQueryParameter("download");
                    if (Objects.equals(res, "0")) {
                        try {
                            EVENTS.getInstance(requireContext())
                                    .warning(getString(R.string.browser_handle_file, filename));
                        } finally {
                            progressIndicator.setVisibility(View.GONE);
                        }
                    } else {
                        contentDownloader(uri);
                    }
                } else {
                    fileDownloader(uri, filename, mimeType, contentLength);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        EventsModel eventsModel =
                new ViewModelProvider(requireActivity()).get(EventsModel.class);

        eventsModel.uri().observe(getViewLifecycleOwner(), (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        openUri(Uri.parse(content));
                    }
                    eventsModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventsModel.online().observe(getViewLifecycleOwner(), (event) -> {
            try {
                if (event != null) {
                    offline_mode.setVisibility(View.GONE);
                    eventsModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });
        eventsModel.offline().observe(getViewLifecycleOwner(), (event) -> {
            try {
                if (event != null) {
                    offline_mode.setVisibility(View.VISIBLE);
                    eventsModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        webView.setWebViewClient(new WebViewClient() {

            private final Map<Uri, Boolean> loadedUrls = new HashMap<>();
            private final AtomicReference<String> host = new AtomicReference<>();


            @Override
            public void onReceivedHttpError(
                    WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                LogUtils.info(TAG, "onReceivedHttpError " + errorResponse.getReasonPhrase());
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                LogUtils.info(TAG, "onReceivedSslError " + error.toString());
            }


            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
                LogUtils.info(TAG, "onPageCommitVisible " + url);
                progressIndicator.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {

                try {

                    WebViewDatabase database = WebViewDatabase.getInstance(requireContext());
                    String[] data = database.getHttpAuthUsernamePassword(host, realm);


                    String storedName = null;
                    String storedPass = null;

                    if (data != null) {
                        storedName = data[0];
                        storedPass = data[1];
                    }

                    LayoutInflater inflater = (LayoutInflater)
                            requireActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View form = inflater.inflate(R.layout.http_auth_request, null);


                    final EditText usernameInput = form.findViewById(R.id.user_name);
                    final EditText passwordInput = form.findViewById(R.id.password);

                    if (storedName != null) {
                        usernameInput.setText(storedName);
                    }

                    if (storedPass != null) {
                        passwordInput.setText(storedPass);
                    }

                    MaterialAlertDialogBuilder authDialog = new MaterialAlertDialogBuilder(
                            requireContext())
                            .setTitle(R.string.authentication)
                            .setView(form)
                            .setCancelable(false)
                            .setPositiveButton(android.R.string.ok, (dialog, whichButton) -> {

                                String username = usernameInput.getText().toString();
                                String password = passwordInput.getText().toString();

                                database.setHttpAuthUsernamePassword(host, realm, username, password);

                                handler.proceed(username, password);
                                dialog.dismiss();
                            })

                            .setNegativeButton(android.R.string.cancel, (dialog, whichButton) -> {
                                dialog.dismiss();
                                view.stopLoading();
                                handler.cancel();
                            });


                    authDialog.show();
                    return;
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

                super.onReceivedHttpAuthRequest(view, handler, host, realm);
            }


            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                LogUtils.info(TAG, "onLoadResource : " + url);
            }

            @Override
            public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
                super.doUpdateVisitedHistory(view, url, isReload);
                LogUtils.info(TAG, "doUpdateVisitedHistory : " + url + " " + isReload);
            }

            @Override
            public void onPageStarted(WebView view, String uri, Bitmap favicon) {
                LogUtils.info(TAG, "onPageStarted : " + uri);

                progressIndicator.setVisibility(View.VISIBLE);
                releaseActionMode();
                requireActivity().invalidateOptionsMenu();

            }


            @Override
            public void onPageFinished(WebView view, String url) {
                LogUtils.info(TAG, "onPageFinished : " + url);

                Uri uri = Uri.parse(url);
                if (Objects.equals(uri.getScheme(), Settings.IPNS) ||
                        Objects.equals(uri.getScheme(), Settings.IPFS)) {

                    try {
                        if (DOCS.getInstance(requireContext()).numUris() == 0) {
                            progressIndicator.setVisibility(View.GONE);
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                } else {
                    progressIndicator.setVisibility(View.GONE);
                }

            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                LogUtils.info(TAG, "onReceivedError " + view.getUrl() + " " + error.getDescription());
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

                try {
                    Uri uri = request.getUrl();

                    LogUtils.error(TAG, "shouldOverrideUrlLoading : " + uri);

                    if (Objects.equals(uri.getScheme(), Settings.ABOUT)) {
                        return true;
                    } else if (Objects.equals(uri.getScheme(), Settings.HTTP)) {

                        Uri redirectUri = DOCS.redirectHttp(uri);
                        if (!Objects.equals(redirectUri, uri)) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, redirectUri,
                                    requireContext(), MainActivity.class);
                            startActivity(intent);
                            return true;
                        }
                        return false;
                    } else if (Objects.equals(uri.getScheme(), Settings.HTTPS)) {
                        if (Settings.isRedirectUrlEnabled(requireContext())) {
                            Uri redirectUri = DOCS.redirectHttps(uri);
                            if (!Objects.equals(redirectUri, uri)) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, redirectUri,
                                        requireContext(), MainActivity.class);
                                startActivity(intent);
                                return true;
                            }
                        }
                        return false;
                    } else if (Objects.equals(uri.getScheme(), Settings.MAGNET)) {

                        MagnetUri magnetUri = MagnetUriParser.lenientParser().parse(uri.toString());

                        String name = uri.toString();
                        if (magnetUri.getDisplayName().isPresent()) {
                            name = magnetUri.getDisplayName().get();
                        }
                        magnetDownloader(uri, name);

                        return true;
                    } else if (Objects.equals(uri.getScheme(), Settings.IPNS) ||
                            Objects.equals(uri.getScheme(), Settings.IPFS)) {

                        String res = uri.getQueryParameter("download");
                        if (Objects.equals(res, "1")) {
                            contentDownloader(uri);
                            return true;
                        }

                        progressIndicator.setVisibility(View.VISIBLE);
                        return false;
                    } else if (Objects.equals(uri.getScheme(), Settings.MAGNET)) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(intent);
                        } catch (Throwable ignore) {
                            EVENTS.getInstance(requireContext()).warning(
                                    getString(R.string.no_activity_found_to_handle_uri));
                        }
                        return true;
                    } else {
                        // all other stuff
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } catch (Throwable ignore) {
                            LogUtils.error(TAG, "Not  handled uri " + uri);
                        }
                        return true;
                    }

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

                return false;

            }


            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {

                Uri uri = request.getUrl();

                LogUtils.error(TAG, "shouldInterceptRequest : " + uri.toString());

                host.set(uri.getHost());
                if (Objects.equals(uri.getScheme(), Settings.HTTP) ||
                        Objects.equals(uri.getScheme(), Settings.HTTPS)) {
                    boolean advertisement = false;
                    if (!loadedUrls.containsKey(uri)) {
                        advertisement = AdBlocker.isAd(uri);
                        loadedUrls.put(uri, advertisement);
                    } else {
                        Boolean value = loadedUrls.get(uri);
                        if (value != null) {
                            advertisement = value;
                        }
                    }

                    if (advertisement) {
                        try {
                            return DOCS.createEmptyResource();
                        } catch (Throwable throwable) {
                            return null;
                        }
                    } else {
                        return null;
                    }

                } else if (Objects.equals(uri.getScheme(), Settings.IPNS) ||
                        Objects.equals(uri.getScheme(), Settings.IPFS)) {

                    try {
                        DOCS docs = DOCS.getInstance(requireContext());
                        docs.attachUri(uri);

                        int authority = uri.getAuthority().hashCode();
                        Session session = docs.getSession(authority);

                        Cancellable cancellable = () -> !docs.hasSession(authority);

                        try {
                            Uri redirectUri = uri;
                            if (Settings.isRedirectIndexEnabled(requireContext())) {
                                redirectUri = docs.redirectUri(session, uri, cancellable);
                                if (!Objects.equals(uri, redirectUri)) {
                                    return docs.createRedirectMessage(redirectUri);
                                }
                            }

                            return docs.getResponse(session, redirectUri, cancellable);

                        } catch (Throwable throwable) {
                            if (cancellable.isCancelled()) {
                                return DOCS.createEmptyResource();
                            }
                            return DOCS.createErrorMessage(throwable);
                        } finally {
                            docs.detachUri(uri);
                        }
                    } catch (Throwable throwable) {
                        return DOCS.createErrorMessage(throwable);
                    }
                }
                return null;
            }
        });

        invalidateOptionsMenu();
        if (savedInstanceState != null) {
            webView.restoreState(savedInstanceState);
        }
    }

    public boolean hasBookmark() {
        BOOKS books = BOOKS.getInstance(requireContext());

        String url = getWebView().getUrl();
        if (url != null && !url.isEmpty()) {
            return books.hasBookmark(url);
        } else {
            return false;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        webView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        webView.onResume();
    }

    public boolean onBackPressedCheck() {

        if (webView.canGoBack()) {
            previousPage();
            return true;
        }
        return false;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        webView.saveState(outState);
    }

    @NonNull
    public WebView getWebView() {
        return webView;
    }

    public void clearWebView() {
        webView.clearHistory();
        webView.clearCache(true);
        webView.clearFormData();

    }

    public boolean downloadActive() {
        try {
            String url = webView.getUrl();
            if (url != null && !url.isEmpty()) {
                Uri uri = Uri.parse(url);
                if (Objects.equals(uri.getScheme(), Settings.IPFS) ||
                        Objects.equals(uri.getScheme(), Settings.IPNS)) {
                    return true;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    private void contentDownloader(@NonNull Uri uri) {

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
        builder.setTitle(R.string.download_title);
        builder.setMessage(DOCS.getFileName(uri));

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {
            DownloadContentWorker.download(requireContext(), uri);

            progressIndicator.setVisibility(View.GONE);
        });
        builder.setNeutralButton(getString(android.R.string.cancel),
                (dialog, which) -> {
                    progressIndicator.setVisibility(View.GONE);
                    dialog.cancel();
                });
        builder.show();


    }

    private void magnetDownloader(@NonNull Uri uri, @NonNull String name) {


        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
        builder.setTitle(R.string.download_title);
        builder.setMessage(name);

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {

            DownloadMagnetWorker.download(requireContext(), uri);

            progressIndicator.setVisibility(View.GONE);
        });
        builder.setNeutralButton(getString(android.R.string.cancel),
                (dialog, which) -> {
                    progressIndicator.setVisibility(View.GONE);
                    dialog.cancel();
                });
        builder.show();


    }

    private void fileDownloader(@NonNull Uri uri, @NonNull String filename,
                                @NonNull String mimeType, long size) {

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
        builder.setTitle(R.string.download_title);
        builder.setMessage(filename);

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {

            DownloadFileWorker.download(requireContext(), uri, filename, mimeType, size);

            progressIndicator.setVisibility(View.GONE);

        });
        builder.setNeutralButton(getString(android.R.string.cancel),
                (dialog, which) -> {
                    progressIndicator.setVisibility(View.GONE);
                    dialog.cancel();
                });
        builder.show();

    }

    public void reloadPage() {

        try {
            progressIndicator.setVisibility(View.GONE);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        try {
            DOCS.getInstance(requireContext())
                    .cleanupResolver(Uri.parse(webView.getUrl()));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        try {
            webView.reload();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    private void openUri(@NonNull Uri uri) {
        try {
            progressIndicator.setVisibility(View.VISIBLE);

            DOCS docs = DOCS.getInstance(requireContext());
            if (Objects.equals(uri.getScheme(), Settings.IPNS) ||
                    Objects.equals(uri.getScheme(), Settings.IPFS)) {

                docs.cleanupResolver(uri);
                docs.attachUri(uri);
            }
            webView.getSettings().setJavaScriptEnabled(
                    Settings.isJavascriptEnabled(requireContext())
            );
            docs.releaseSessions(uri.getAuthority().hashCode());
            webView.stopLoading();
            webView.loadUrl(uri.toString());

            requireActivity().invalidateOptionsMenu();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    public void download() {
        try {
            String url = webView.getUrl();
            if (url != null && !url.isEmpty()) {
                Uri uri = Uri.parse(url);
                if (Objects.equals(uri.getScheme(), Settings.IPFS) ||
                        Objects.equals(uri.getScheme(), Settings.IPNS)) {
                    contentDownloader(uri);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void share() {
        try {
            Uri uri = Uri.parse(webView.getUrl());

            ComponentName[] names = {new ComponentName(requireContext(), MainActivity.class)};

            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_link));
            intent.putExtra(Intent.EXTRA_TEXT, uri.toString());
            intent.setType(MimeTypeService.PLAIN_MIME_TYPE);
            intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);


            Intent chooser = Intent.createChooser(intent, getText(R.string.share));
            chooser.putExtra(Intent.EXTRA_EXCLUDE_COMPONENTS, names);
            startActivity(chooser);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void previousPage() {
        try {
            webView.stopLoading();
            webView.goBack();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void nextPage() {
        try {
            webView.stopLoading();
            webView.goForward();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void info() {
        try {
            View view = LayoutInflater.from(requireContext()).inflate(
                    R.layout.content_info, null);

            ImageView imageView = view.findViewById(R.id.qr_code_image);
            String title = getString(R.string.information);
            String message = getString(R.string.url_access);
            String uri = webView.getUrl();

            TextView page = view.findViewById(R.id.page);
            if (uri.isEmpty()) {
                page.setVisibility(View.GONE);
            } else {
                page.setText(uri);
            }


            Bitmap bitmap = DOCS.getQRCode(uri);
            imageView.setImageBitmap(bitmap);

            MaterialAlertDialogBuilder builder =
                    new MaterialAlertDialogBuilder(requireContext());

            builder.setTitle(title).setMessage(message).setView(view).create().show();


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private ActionMode.Callback createFindActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_find_action_mode, menu);

                MenuItem action_mode_find = menu.findItem(R.id.action_mode_find);
                TextInputEditText mEditText = (TextInputEditText) action_mode_find.getActionView();

                mEditText.setSingleLine();
                mEditText.setHint(R.string.enter_search);
                mEditText.setFocusable(true);
                mEditText.requestFocus();

                mEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        webView.findAllAsync(editable.toString());
                    }
                });

                mode.setTitle("0/0");

                webView.setFindListener((activeMatchOrdinal, numberOfMatches, isDoneCounting) -> {
                    try {
                        String result = "" + activeMatchOrdinal + "/" + numberOfMatches;
                        mode.setTitle(result);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                });

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

                if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                    return true;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                int itemId = item.getItemId();

                if (itemId == R.id.action_mode_previous) {
                    try {
                        webView.findNext(false);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                    return true;
                } else if (itemId == R.id.action_mode_next) {
                    try {
                        webView.findNext(true);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                    return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                try {
                    webView.clearMatches();
                    webView.setFindListener(null);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                } finally {
                    actionMode = null;
                }
            }
        };

    }

    private void releaseActionMode() {
        try {
            if (actionMode != null) {
                actionMode.finish();
                actionMode = null;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void findPage() {
        try {
            actionMode = ((AppCompatActivity) requireActivity()).startSupportActionMode(
                    createFindActionModeCallback());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private Dialog getDialog() {
        if (widthWindowSizeClass == WindowSizeClass.EXPANDED) {
            return new SideSheetDialog(requireContext());
        } else {
            BottomSheetDialog dialog = new BottomSheetDialog(requireContext());
            BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            behavior.setPeekHeight(0);
            return dialog;
        }
    }

    public void history() {
        try {

            Dialog historyDialog = getDialog();
            historyDialog.setContentView(R.layout.fragment_history);

            RecyclerView history = historyDialog.findViewById(R.id.history);
            Objects.requireNonNull(history);


            history.setLayoutManager(new LinearLayoutManager(requireContext()));
            HistoryAdapter mHistoryAdapter = new HistoryAdapter(uri -> {

                try {
                    EVENTS.getInstance(requireContext()).uri(uri);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                } finally {
                    historyDialog.dismiss();
                }
            }, webView.copyBackForwardList());
            history.setAdapter(mHistoryAdapter);
            historyDialog.show();

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Nullable
    public String getUrl() {
        return webView.getUrl();
    }

    @Nullable
    public String getTitle() {
        return webView.getTitle();
    }

    public void createBitmap(@NonNull Consumer<Bitmap> consumer) {

        int[] locationOfViewInWindow = new int[2];
        webView.getLocationInWindow(locationOfViewInWindow);

        int min = Math.min(webView.getWidth(), webView.getHeight());

        Bitmap bitmap = Bitmap.createBitmap(min, min, Bitmap.Config.ARGB_8888);

        PixelCopy.request(requireActivity().getWindow(),

                new Rect(
                        locationOfViewInWindow[0],
                        locationOfViewInWindow[1],
                        locationOfViewInWindow[0] + min,
                        locationOfViewInWindow[1] + min
                ),

                bitmap, copyResult -> {
                    try {
                        if (copyResult == PixelCopy.SUCCESS) {
                            consumer.accept(bitmap);
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                },
                new Handler(Looper.getMainLooper()));

    }

    public enum WindowSizeClass {COMPACT, MEDIUM, EXPANDED}
}
