package threads.thor.utils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import java.util.List;
import java.util.Objects;

import threads.thor.core.tabs.Tab;


@SuppressWarnings("WeakerAccess")
public class TabDiffCallback extends DiffUtil.Callback {
    private final List<Tab> mOldList;
    private final List<Tab> mNewList;

    public TabDiffCallback(List<Tab> tabs, List<Tab> newTabs) {
        this.mOldList = tabs;
        this.mNewList = newTabs;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return areItemsTheSame(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return sameContent(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }


    private boolean sameContent(@NonNull Tab t, @NonNull Tab o) {
        return Objects.equals(t.getUri(), o.getUri()) &&
                Objects.equals(t.getTitle(), o.getTitle()) &&
                Objects.equals(t.getBitmap(), o.getBitmap());
    }

    private boolean areItemsTheSame(@NonNull Tab t, @NonNull Tab o) {
        return t.getIdx() == o.getIdx();
    }

}
