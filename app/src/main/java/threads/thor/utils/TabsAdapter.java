package threads.thor.utils;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.behavior.SwipeDismissBehavior;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.imageview.ShapeableImageView;

import java.util.ArrayList;
import java.util.List;

import threads.thor.R;
import threads.thor.core.tabs.Tab;

public class TabsAdapter extends RecyclerView.Adapter<TabsAdapter.ImageViewHolder> {

    private final ViewHolderListener viewHolderListener;
    private final List<Tab> tabs = new ArrayList<>();

    public TabsAdapter(ViewHolderListener viewHolderListener) {
        this.viewHolderListener = viewHolderListener;
    }

    public void updateData(@NonNull List<Tab> tabs) {
        final TabDiffCallback diffCallback = new TabDiffCallback(this.tabs, tabs);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.tabs.clear();
        this.tabs.addAll(tabs);
        diffResult.dispatchUpdatesTo(this);
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_tab, parent, false);
        return new ImageViewHolder(view, viewHolderListener);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        holder.onBind(tabs.get(position));
    }

    @Override
    public int getItemCount() {
        return tabs.size();
    }


    public interface ViewHolderListener {

        void onItemClicked(@NonNull Tab tab);

        void onItemDismiss(@NonNull Tab tab);

        boolean isSelected(@NonNull Tab tab);
    }


    static class ImageViewHolder extends RecyclerView.ViewHolder {

        private final ShapeableImageView image;
        private final TextView title;
        private final MaterialCardView cardView;

        private final ViewHolderListener viewHolderListener;
        private Tab tab;

        ImageViewHolder(View itemView, ViewHolderListener listener) {
            super(itemView);
            this.cardView = itemView.findViewById(R.id.card_view);
            this.image = itemView.findViewById(R.id.card_image);
            this.title = itemView.findViewById(R.id.card_title);
            this.viewHolderListener = listener;
            this.cardView.setOnClickListener(v -> listener.onItemClicked(tab));


            SwipeDismissBehavior<View> swipeDismissBehavior = new SwipeDismissBehavior<>();
            swipeDismissBehavior.setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_END_TO_START);


            CoordinatorLayout.LayoutParams coordinatorParams =
                    (CoordinatorLayout.LayoutParams) cardView.getLayoutParams();

            coordinatorParams.setBehavior(swipeDismissBehavior);

            swipeDismissBehavior.setListener(new SwipeDismissBehavior.OnDismissListener() {
                @Override
                public void onDismiss(View view) {
                    listener.onItemDismiss(tab);
                }

                @Override
                public void onDragStateChanged(int state) {
                    switch (state) {
                        case SwipeDismissBehavior.STATE_DRAGGING:
                        case SwipeDismissBehavior.STATE_SETTLING:
                            cardView.setDragged(true);
                            break;
                        case SwipeDismissBehavior.STATE_IDLE:
                            cardView.setDragged(false);
                            break;
                        default:
                    }
                }
            });

        }

        void onBind(Tab tab) {
            this.tab = tab;
            image.setImageBitmap(tab.getBitmap());
            title.setText(tab.getTitle());
            cardView.setChecked(viewHolderListener.isSelected(tab));
        }
    }

}