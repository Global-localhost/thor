package threads.thor.utils;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.behavior.SwipeDismissBehavior;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;
import java.util.List;

import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.core.books.Bookmark;


public class BookmarksAdapter extends RecyclerView.Adapter<BookmarksAdapter.ViewHolder> {
    private static final String TAG = BookmarksAdapter.class.getSimpleName();
    private final BookmarkListener bookmarkListener;
    private final List<Bookmark> bookmarks = new ArrayList<>();

    public BookmarksAdapter(@NonNull BookmarkListener bookmarkListener) {
        this.bookmarkListener = bookmarkListener;
    }


    public void updateData(@NonNull List<Bookmark> bookmarks) {
        final BookmarkDiffCallback diffCallback = new BookmarkDiffCallback(this.bookmarks, bookmarks);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.bookmarks.clear();
        this.bookmarks.addAll(bookmarks);
        diffResult.dispatchUpdatesTo(this);
    }


    @Override
    public int getItemViewType(int position) {
        return R.layout.card_bookmarks;
    }

    @Override
    @NonNull
    public BookmarksAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                          int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new ViewHolder(v, bookmarkListener);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Bookmark bookmark = bookmarks.get(position);
        holder.onBind(bookmark);
    }


    @Override
    public int getItemCount() {
        return bookmarks.size();
    }


    public interface BookmarkListener {
        void onClick(@NonNull Bookmark bookmark);

        void onDismiss(@NonNull Bookmark bookmark);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView bookmark_uri;
        private final TextView bookmark_title;
        private final ImageView bookmark_image;
        private final MaterialCardView cardView;

        private Bookmark bookmark;

        ViewHolder(View v, @NonNull BookmarkListener listener) {
            super(v);

            this.cardView = itemView.findViewById(R.id.card_view);
            this.bookmark_title = itemView.findViewById(R.id.bookmark_title);
            this.bookmark_uri = itemView.findViewById(R.id.bookmark_uri);
            this.bookmark_image = itemView.findViewById(R.id.bookmark_image);


            cardView.setOnClickListener((view) -> {
                try {
                    listener.onClick(bookmark);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

            });


            SwipeDismissBehavior<View> swipeDismissBehavior = new SwipeDismissBehavior<>();
            swipeDismissBehavior.setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_END_TO_START);


            CoordinatorLayout.LayoutParams coordinatorParams =
                    (CoordinatorLayout.LayoutParams) cardView.getLayoutParams();

            coordinatorParams.setBehavior(swipeDismissBehavior);

            swipeDismissBehavior.setListener(new SwipeDismissBehavior.OnDismissListener() {
                @Override
                public void onDismiss(View view) {
                    listener.onDismiss(bookmark);
                }

                @Override
                public void onDragStateChanged(int state) {
                    switch (state) {
                        case SwipeDismissBehavior.STATE_DRAGGING:
                        case SwipeDismissBehavior.STATE_SETTLING:
                            cardView.setDragged(true);
                            break;
                        case SwipeDismissBehavior.STATE_IDLE:
                            cardView.setDragged(false);
                            break;
                        default:
                    }
                }
            });
        }

        void onBind(Bookmark bookmark) {

            this.bookmark = bookmark;

            try {
                String title = bookmark.getTitle();
                bookmark_title.setText(title);
                bookmark_uri.setText(bookmark.getUri());


                Bitmap image = bookmark.getBitmapIcon();
                if (image != null) {
                    bookmark_image.clearColorFilter();
                    bookmark_image.setImageBitmap(image);
                } else {
                    bookmark_image.setImageResource(R.drawable.bookmark);
                    if (!title.isEmpty()) {
                        int color = ColorGenerator.MATERIAL.getColor(title);
                        bookmark_image.setColorFilter(color);
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

    }
}
