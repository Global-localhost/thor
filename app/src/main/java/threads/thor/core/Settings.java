package threads.thor.core;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;


public class Settings {


    public static final String NAME = "name";
    public static final String IPFS = "ipfs";
    public static final String IPNS = "ipns";
    public static final String SIZE = "size";
    public static final String TYPE = "type";
    public static final String HTTPS = "https";
    public static final String ABOUT = "about";
    public static final String HTTP = "http";
    public static final String UTF8 = "UTF-8";
    public static final String FILE = "file";
    public static final String MAGNET = "magnet";

    public static final String SERVICE = "_p2p._udp";
    public static final String HOMEPAGE = "https://start.duckduckgo.com/";

    private static final String APP_KEY = "AppKey";
    private static final String JAVASCRIPT_KEY = "javascriptKey";
    private static final String REDIRECT_URL_KEY = "redirectUrlKey";
    private static final String REDIRECT_INDEX_KEY = "redirectIndexKey";

    public static void setJavascriptEnabled(Context context, boolean auto) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(JAVASCRIPT_KEY, auto);
        editor.apply();
    }

    public static boolean isJavascriptEnabled(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(JAVASCRIPT_KEY, true);

    }

    public static void setRedirectUrlEnabled(Context context, boolean auto) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(REDIRECT_URL_KEY, auto);
        editor.apply();
    }

    public static boolean isRedirectUrlEnabled(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(REDIRECT_URL_KEY, false);
    }

    public static void setRedirectIndexEnabled(Context context, boolean auto) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(REDIRECT_INDEX_KEY, auto);
        editor.apply();
    }

    public static boolean isRedirectIndexEnabled(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(REDIRECT_INDEX_KEY, true);

    }
}
