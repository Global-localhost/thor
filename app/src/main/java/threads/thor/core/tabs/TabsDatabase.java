package threads.thor.core.tabs;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@androidx.room.Database(entities = {Tab.class}, version = 1, exportSchema = false)
@TypeConverters({Tab.class})
public abstract class TabsDatabase extends RoomDatabase {

    public abstract TabDao tabDao();

}
