package threads.thor.core.tabs;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import java.io.ByteArrayOutputStream;
import java.util.Objects;

@androidx.room.Entity
public class Tab {
    @Nullable
    @ColumnInfo(name = "uri")
    private final String uri;
    @Nullable
    @ColumnInfo(name = "title")
    private final String title;
    @Nullable
    @ColumnInfo(name = "bitmap", typeAffinity = ColumnInfo.BLOB)
    @TypeConverters(Tab.class)
    private final Bitmap bitmap;
    @PrimaryKey(autoGenerate = true)
    private long idx;

    public Tab(@Nullable String title, @Nullable String uri, @Nullable Bitmap bitmap) {
        this.uri = uri;
        this.title = title;
        this.bitmap = bitmap;
    }

    @NonNull
    public static Tab createTab(@Nullable String title, @Nullable String uri, @Nullable Bitmap bitmap) {
        return new Tab(title, uri, bitmap);
    }

    @Nullable
    @TypeConverter
    public static Bitmap fromArray(byte[] data) {
        if (data == null) {
            return null;
        }
        return BitmapFactory.decodeByteArray(data, 0, data.length);
    }

    @Nullable
    @TypeConverter
    public static byte[] toArray(@Nullable Bitmap bitmap) {
        try {
            if (bitmap == null) {
                return null;
            }
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 10, stream);
            return stream.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tab tab = (Tab) o;
        return idx == tab.idx;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idx);
    }

    @Nullable
    public Bitmap getBitmap() {
        return bitmap;
    }

    @Nullable
    public String getUri() {
        return uri;
    }

    @Nullable
    public String getTitle() {
        return title;
    }

    public long getIdx() {
        return idx;
    }

    public void setIdx(long idx) {
        this.idx = idx;
    }

}
