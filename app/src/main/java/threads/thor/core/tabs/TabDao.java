package threads.thor.core.tabs;

import android.graphics.Bitmap;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface TabDao {

    @Query("SELECT * FROM Tab")
    LiveData<List<Tab>> getLiveDataTabs();

    @Query("SELECT * FROM Tab")
    List<Tab> getTabs();

    @Query("SELECT * FROM Tab WHERE idx = :idx")
    Tab getTab(long idx);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertTab(Tab tab);

    @Delete
    void deleteTab(Tab tab);

    @Query("UPDATE Tab SET title = :title, uri = :uri, bitmap = :bitmap WHERE idx = :idx")
    void updateTab(long idx, String title, String uri, Bitmap bitmap);

    @Query("SELECT EXISTS(SELECT * FROM Tab)")
    boolean hasTabs();
}
