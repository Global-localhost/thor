package threads.thor.core.tabs;

import android.content.Context;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.List;

import threads.thor.core.events.EVENTS;

public class TABS {
    private static volatile TABS INSTANCE = null;

    private final TabsDatabase tabsDatabase;

    private TABS(TabsDatabase tabsDatabase) {
        this.tabsDatabase = tabsDatabase;
    }

    public static TABS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (EVENTS.class) {
                if (INSTANCE == null) {
                    TabsDatabase tabsDatabase =
                            Room.inMemoryDatabaseBuilder(context,
                                            TabsDatabase.class).
                                    allowMainThreadQueries().build();
                    INSTANCE = new TABS(tabsDatabase);
                }
            }
        }
        return INSTANCE;
    }


    public long createTab(@Nullable String title, @Nullable String uri, @Nullable Bitmap bitmap) {
        Tab tab = Tab.createTab(title, uri, bitmap);
        return tabsDatabase.tabDao().insertTab(tab);
    }

    @Nullable
    public Tab getTab(long idx) {
        return tabsDatabase.tabDao().getTab(idx);
    }

    @NonNull
    public List<Tab> getTabs() {
        return tabsDatabase.tabDao().getTabs();
    }

    @NonNull
    public TabsDatabase getTabsDatabase() {
        return tabsDatabase;
    }

    public void updateTab(long tab, @Nullable String title,
                          @Nullable String uri, @NonNull Bitmap bitmap) {
        tabsDatabase.tabDao().updateTab(tab, title, uri, bitmap);
    }

    public void clear() {
        tabsDatabase.clearAllTables();
    }

    public void removeTab(@NonNull Tab tab) {
        tabsDatabase.tabDao().deleteTab(tab);
    }

    public boolean hasTabs() {
        return tabsDatabase.tabDao().hasTabs();
    }
}
