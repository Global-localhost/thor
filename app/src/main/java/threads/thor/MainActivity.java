package threads.thor;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowMetricsCalculator;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.search.SearchBar;
import com.google.android.material.sidesheet.SideSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.journeyapps.barcodescanner.ScanContract;
import com.journeyapps.barcodescanner.ScanOptions;

import java.util.Objects;

import threads.lite.cid.Cid;
import threads.thor.core.DOCS;
import threads.thor.core.Settings;
import threads.thor.core.books.BOOKS;
import threads.thor.core.books.Bookmark;
import threads.thor.core.events.EVENTS;
import threads.thor.core.tabs.TABS;
import threads.thor.core.tabs.Tab;
import threads.thor.fragments.BookmarksFragment;
import threads.thor.fragments.BrowserFragment;
import threads.thor.fragments.TabsFragment;
import threads.thor.model.EventsModel;
import threads.thor.model.TabsModel;
import threads.thor.services.MimeTypeService;
import threads.thor.utils.PermissionAction;
import threads.thor.work.BrowserResetWorker;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final long CLICK_OFFSET = 500;
    private final ActivityResultLauncher<Intent> mFolderRequestForResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();

                    try {
                        Objects.requireNonNull(data);
                        Uri uri = data.getData();
                        Objects.requireNonNull(uri);


                        String mimeType = getContentResolver().getType(uri);

                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(uri, mimeType);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                        startActivity(intent);

                    } catch (Throwable e) {
                        EVENTS.getInstance(getApplicationContext()).warning(
                                getString(R.string.no_activity_found_to_handle_uri));
                    }
                }
            });
    private final ActivityResultLauncher<ScanOptions>
            mScanRequestForResult = registerForActivityResult(new ScanContract(),
            result -> {
                if (result.getContents() != null) {
                    try {
                        Uri uri = Uri.parse(result.getContents());
                        if (uri != null) {
                            String scheme = uri.getScheme();
                            if (Objects.equals(scheme, Settings.IPNS) ||
                                    Objects.equals(scheme, Settings.IPFS) ||
                                    Objects.equals(scheme, Settings.HTTP) ||
                                    Objects.equals(scheme, Settings.HTTPS)) {
                                EVENTS.getInstance(getApplicationContext()).uri(uri.toString());
                            } else {
                                EVENTS.getInstance(getApplicationContext()).error(
                                        getString(R.string.codec_not_supported));
                            }
                        } else {
                            EVENTS.getInstance(getApplicationContext()).error(
                                    getString(R.string.codec_not_supported));
                        }
                    } catch (Throwable throwable) {
                        EVENTS.getInstance(getApplicationContext()).error(
                                getString(R.string.codec_not_supported));
                    }
                }
            });
    private final ActivityResultLauncher<String> requestPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                if (isGranted) {
                    invokeScan();
                } else {
                    EVENTS.getInstance(getApplicationContext()).permission(
                            getString(R.string.permission_camera_denied));
                }
            });
    private BrowserFragment browserFragment;
    private WindowSizeClass widthWindowSizeClass = WindowSizeClass.COMPACT;
    private CoordinatorLayout coordinatorLayout;
    private TabsModel tabsModel;
    private long lastClickTime = 0;

    private boolean hasCamera;
    private SearchBar searchBar;

    private void computeWindowSizeClasses() {
        WindowMetrics metrics = WindowMetricsCalculator.getOrCreate()
                .computeCurrentWindowMetrics(this);

        float widthDp = metrics.getBounds().width() /
                getResources().getDisplayMetrics().density;


        if (widthDp < 600f) {
            widthWindowSizeClass = WindowSizeClass.COMPACT;
        } else if (widthDp < 840f) {
            widthWindowSizeClass = WindowSizeClass.MEDIUM;
        } else {
            widthWindowSizeClass = WindowSizeClass.EXPANDED;
        }

    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        try {
            computeWindowSizeClasses();
            invalidateOptionsMenu();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private Dialog getDialog() {
        if (widthWindowSizeClass == WindowSizeClass.EXPANDED) {
            return new SideSheetDialog(this);
        } else {
            BottomSheetDialog dialog = new BottomSheetDialog(this);
            BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            behavior.setPeekHeight(0);
            return dialog;
        }
    }


    private void invokeScan() {
        try {
            PackageManager pm = getPackageManager();

            if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
                ScanOptions options = new ScanOptions();
                options.setDesiredBarcodeFormats(ScanOptions.ALL_CODE_TYPES);
                options.setPrompt(getString(R.string.scan_qrcode));
                options.setCameraId(0);  // Use a specific camera of the device
                options.setBeepEnabled(true);
                options.setOrientationLocked(false);
                mScanRequestForResult.launch(options);
            } else {
                EVENTS.getInstance(getApplicationContext()).permission(
                        getString(R.string.feature_camera_required));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    private String prettyUri(@NonNull Uri uri, @NonNull String replace) {
        return uri.toString().replaceFirst(replace, "");
    }

    private void updateTitle() {
        try {
            String url = browserFragment.getUrl();
            if (url != null) {
                BOOKS books = BOOKS.getInstance(getApplicationContext());
                Bookmark bookmark = books.getBookmark(url);

                String title = url;
                if (bookmark != null) {
                    String bookmarkTitle = bookmark.getTitle();
                    if (!bookmarkTitle.isEmpty()) {
                        title = bookmarkTitle;
                    }
                } else {
                    Uri uri = Uri.parse(url);
                    if (Objects.equals(uri.getScheme(), Settings.HTTPS)) {
                        title = prettyUri(uri, "https://");
                    } else if (Objects.equals(uri.getScheme(), Settings.HTTP)) {
                        title = prettyUri(uri, "http://");
                    }
                }
                searchBar.clearText();
                searchBar.setHint(title);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuCompat.setGroupDividerEnabled(menu, true);

        updateTitle();

        try {

            if (widthWindowSizeClass == WindowSizeClass.EXPANDED) {

                boolean hasBookmark = browserFragment.hasBookmark();
                if (hasBookmark) {
                    menu.findItem(R.id.action_bookmark).setIcon(R.drawable.star);
                } else {
                    menu.findItem(R.id.action_bookmark).setIcon(R.drawable.star_outline);
                }
            }

            if (!hasCamera) { // no camera no scan
                menu.findItem(R.id.action_scan).setVisible(false);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                return true;
            }
            lastClickTime = SystemClock.elapsedRealtime();


            browserFragment.createBitmap(bitmap -> {


                TABS tabs = TABS.getInstance(getApplicationContext());
                long tab = tabsModel.getCurrentTabValue();
                if (tab == 0L) {
                    if (tabs.hasTabs()) {
                        // take the first one, no selection was set before
                        tab = tabs.getTabs().get(0).getIdx();
                        TABS.getInstance(getApplicationContext()).updateTab(tab,
                                browserFragment.getTitle(),
                                browserFragment.getUrl(),
                                bitmap);
                        tabsModel.setCurrentTab(tab);
                    } else {
                        tab = tabs.createTab(
                                browserFragment.getTitle(),
                                browserFragment.getUrl(),
                                bitmap);
                        tabsModel.setCurrentTab(tab);
                    }
                } else {
                    TABS.getInstance(getApplicationContext()).updateTab(tab,
                            browserFragment.getTitle(),
                            browserFragment.getUrl(),
                            bitmap);
                }
                try {
                    TabsFragment.newInstance()
                            .show(getSupportFragmentManager(), TabsFragment.TAG);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            });


            return true;
        } else if (itemId == R.id.action_bookmarks) {
            if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                return true;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            try {
                BookmarksFragment.newInstance()
                        .show(getSupportFragmentManager(), BookmarksFragment.TAG);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

            return true;
        } else if (itemId == R.id.action_overflow) {
            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                    return true;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                LayoutInflater inflater = (LayoutInflater)
                        getSystemService(LAYOUT_INFLATER_SERVICE);


                View menuOverflow = inflater.inflate(
                        R.layout.menu_overflow, coordinatorLayout, false);


                PopupWindow dialog = new PopupWindow(
                        MainActivity.this, null, android.R.attr.contextPopupMenuStyle);
                dialog.setContentView(menuOverflow);
                dialog.setOutsideTouchable(true);
                dialog.setFocusable(true);

                dialog.showAsDropDown(coordinatorLayout, 0, 0, Gravity.TOP | Gravity.END);


                MaterialButton actionNextPage = menuOverflow.findViewById(R.id.action_next_page);
                actionNextPage.setEnabled(browserFragment.getWebView().canGoForward());
                actionNextPage.setOnClickListener(v1 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        browserFragment.nextPage();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                MaterialButton actionBookmark = menuOverflow.findViewById(R.id.action_bookmark);


                boolean hasBookmark = browserFragment.hasBookmark();
                if (hasBookmark) {
                    actionBookmark.setIconResource(R.drawable.star);
                } else {
                    actionBookmark.setIconResource(R.drawable.star_outline);
                }

                actionBookmark.setOnClickListener(v1 -> {

                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        browserFragment.bookmark();

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                });

                Button actionFindPage = menuOverflow.findViewById(R.id.action_find_page);

                actionFindPage.setOnClickListener(v12 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        browserFragment.findPage();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                Button actionDownload = menuOverflow.findViewById(R.id.action_download);

                actionDownload.setEnabled(browserFragment.downloadActive());

                actionDownload.setOnClickListener(v13 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        browserFragment.download();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                Button actionShare = menuOverflow.findViewById(R.id.action_share);

                actionShare.setOnClickListener(v14 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        browserFragment.share();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionInformation = menuOverflow.findViewById(R.id.action_information);
                actionInformation.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        browserFragment.info();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionScan = menuOverflow.findViewById(R.id.action_scan);
                if (!hasCamera) {
                    actionScan.setVisibility(View.GONE);
                }
                actionScan.setOnClickListener(v -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < 500) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);


                        if (ContextCompat.checkSelfPermission
                                (getApplicationContext(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissionLauncher.launch(Manifest.permission.CAMERA);
                            return;
                        }

                        invokeScan();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                });


                TextView actionHistory = menuOverflow.findViewById(R.id.action_history);
                actionHistory.setOnClickListener(v16 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        browserFragment.history();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionDownloads = menuOverflow.findViewById(R.id.action_downloads);
                actionDownloads.setOnClickListener(v17 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        showDownloads();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionCleanup = menuOverflow.findViewById(R.id.action_cleanup);
                actionCleanup.setOnClickListener(v18 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        cleanup();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionSettings = menuOverflow.findViewById(R.id.action_settings);
                actionSettings.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        showSettings();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionDocumentation = menuOverflow.findViewById(R.id.action_documentation);
                actionDocumentation.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        showDocumentation();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
            return true;
        } else if (itemId == R.id.action_bookmark) {
            browserFragment.bookmark();
            return true;
        } else if (itemId == R.id.action_share) {
            browserFragment.share();
            return true;
        } else if (itemId == R.id.action_scan) {
            if (ContextCompat.checkSelfPermission
                    (getApplicationContext(), Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissionLauncher.launch(Manifest.permission.CAMERA);
                return true;
            }

            invokeScan();
            return true;
        } else if (itemId == R.id.action_history) {
            browserFragment.history();
            return true;
        } else if (itemId == R.id.action_downloads) {
            showDownloads();
            return true;
        } else if (itemId == R.id.action_settings) {
            showSettings();
            return true;
        } else if (itemId == R.id.action_documentation) {
            showDocumentation();
            return true;
        } else if (itemId == R.id.action_cleanup) {
            cleanup();
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    private void cleanup() {
        try {
            // clear web view data
            browserFragment.clearWebView();

            // Clear data and cookies
            BrowserResetWorker.reset(getApplicationContext());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void showDocumentation() {
        try {
            String uri = "https://gitlab.com/remmer.wilts/thor";

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri),
                    getApplicationContext(), MainActivity.class);
            startActivity(intent);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void showSettings() {
        try {
            Dialog settingsDialog = getDialog();

            settingsDialog.setContentView(R.layout.fragment_settings);


            SwitchMaterial enableJavascript = settingsDialog.findViewById(
                    R.id.enable_javascript);
            Objects.requireNonNull(enableJavascript);
            enableJavascript.setChecked(Settings.isJavascriptEnabled(this));
            enableJavascript.setOnCheckedChangeListener((buttonView, isChecked) ->
                    Settings.setJavascriptEnabled(this, isChecked)
            );


            SwitchMaterial enableRedirectUrl = settingsDialog.findViewById(
                    R.id.enable_redirect_url);
            Objects.requireNonNull(enableRedirectUrl);
            enableRedirectUrl.setChecked(Settings.isRedirectUrlEnabled(this));
            enableRedirectUrl.setOnCheckedChangeListener((buttonView, isChecked) ->
                    Settings.setRedirectUrlEnabled(this, isChecked)
            );

            SwitchMaterial enableRedirectIndex = settingsDialog.findViewById(
                    R.id.enable_redirect_index);
            Objects.requireNonNull(enableRedirectIndex);
            enableRedirectIndex.setChecked(Settings.isRedirectIndexEnabled(this));
            enableRedirectIndex.setOnCheckedChangeListener((buttonView, isChecked) ->
                    Settings.setRedirectIndexEnabled(this, isChecked)
            );

            settingsDialog.show();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        computeWindowSizeClasses();

        browserFragment = (BrowserFragment) getSupportFragmentManager()
                .findFragmentById(R.id.browser_container);

        PackageManager pm = getPackageManager();
        hasCamera = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);

        coordinatorLayout = findViewById(R.id.main_layout);

        searchBar = findViewById(R.id.search_bar);
        setSupportActionBar(searchBar);


        searchBar.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            EVENTS.getInstance(getApplicationContext()).uri(Settings.HOMEPAGE);
        });

        tabsModel = new ViewModelProvider(this).get(TabsModel.class);

        tabsModel.getTabs().observe(this, tabs -> {
            if (tabs != null) {
                int size = tabs.size();
                int resId = R.drawable.numeric_9_plus_box_multiple_outline;
                if (size < 10) {
                    switch (size) {
                        case 0:
                            resId = R.drawable.numeric_0_box_multiple_outline;
                            break;
                        case 1:
                            resId = R.drawable.numeric_1_box_multiple_outline;
                            break;
                        case 2:
                            resId = R.drawable.numeric_2_box_multiple_outline;
                            break;
                        case 3:
                            resId = R.drawable.numeric_3_box_multiple_outline;
                            break;
                        case 4:
                            resId = R.drawable.numeric_4_box_multiple_outline;
                            break;
                        case 5:
                            resId = R.drawable.numeric_5_box_multiple_outline;
                            break;
                        case 6:
                            resId = R.drawable.numeric_6_box_multiple_outline;
                            break;
                        case 7:
                            resId = R.drawable.numeric_7_box_multiple_outline;
                            break;
                        case 8:
                            resId = R.drawable.numeric_8_box_multiple_outline;
                            break;
                        case 9:
                            resId = R.drawable.numeric_9_box_multiple_outline;
                            break;
                    }
                }
                searchBar.setNavigationIcon(resId);
            }
        });

        tabsModel.getCurrentTab().observe(this, idx -> {
            try {
                if (idx != null) {
                    TABS tabs = TABS.getInstance(getApplicationContext());
                    Tab tab = tabs.getTab(idx);
                    if (tab != null) {
                        String uri = tab.getUri();
                        if (uri != null) {
                            EVENTS.getInstance(getApplicationContext()).uri(uri);
                        }
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        EventsModel eventsModel =
                new ViewModelProvider(this).get(EventsModel.class);


        eventsModel.fatal().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());

                        snackbar.show();
                    }
                    eventsModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventsModel.error().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, content, Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                    eventsModel.removeEvent(event);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        eventsModel.warning().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, content,
                                Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }
                    eventsModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        eventsModel.permission().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(R.string.settings, new PermissionAction());
                        snackbar.show();

                    }
                    eventsModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventsModel.info().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Toast.makeText(MainActivity.this,
                                content, Toast.LENGTH_SHORT).show();
                    }
                    eventsModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        Intent intent = getIntent();

        if (!handleIntents(intent)) {
            try {
                homepage();
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
    }

    private void homepage() {
        try {
            boolean hasTabs = TABS.getInstance(getApplicationContext()).hasTabs();
            if (!hasTabs) {
                long tab = DOCS.getInstance(getApplicationContext()).createHomepageTab();
                tabsModel.setCurrentTab(tab);
            } else {
                EVENTS.getInstance(getApplicationContext()).uri(Settings.HOMEPAGE);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void uri(@NonNull Uri uri) {
        try {
            boolean hasTabs = TABS.getInstance(getApplicationContext()).hasTabs();
            if (!hasTabs) {
                long tab = DOCS.getInstance(getApplicationContext()).createTab(uri);
                tabsModel.setCurrentTab(tab);
            } else {
                EVENTS.getInstance(getApplicationContext()).uri(uri.toString());
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    private void showDownloads() {
        try {
            mFolderRequestForResult.launch(InitApplication.getDownloadsIntent());
        } catch (Throwable e) {
            EVENTS.getInstance(getApplicationContext()).warning(
                    getString(R.string.no_activity_found_to_handle_uri));
        }
    }


    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntents(intent);
    }

    @Override
    public void onBackPressed() {

        boolean result = browserFragment.onBackPressedCheck();
        if (result) {
            return;
        }
        super.onBackPressed();
    }

    private boolean handleIntents(Intent intent) {

        final String action = intent.getAction();

        if (Intent.ACTION_VIEW.equals(action)) {
            Uri uri = intent.getData();
            if (uri != null) {
                uri(uri); // create a new tab
                return true;
            }
        }

        if (Intent.ACTION_SEND.equals(action)) {
            if (Objects.equals(intent.getType(), MimeTypeService.PLAIN_MIME_TYPE)) {
                String text = intent.getStringExtra(Intent.EXTRA_TEXT);
                return doSearch(text);
            }
        }

        return false;
    }

    private boolean doSearch(@Nullable String query) {
        try {
            if (query != null && !query.isEmpty()) {
                Uri uri = Uri.parse(query);
                String scheme = uri.getScheme();
                if (Objects.equals(scheme, Settings.IPNS) ||
                        Objects.equals(scheme, Settings.IPFS) ||
                        Objects.equals(scheme, Settings.HTTP) ||
                        Objects.equals(scheme, Settings.HTTPS)) {
                    EVENTS.getInstance(getApplicationContext()).uri(uri.toString());
                } else {
                    String search = "https://duckduckgo.com/?q=" + query + "&kp=-1";
                    try {
                        // in case the search is an ipfs string
                        search = Settings.IPFS + "://" + Cid.decode(query);
                    } catch (Throwable ignore) {
                    }
                    EVENTS.getInstance(getApplicationContext()).uri(search);
                }
                return true;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }


    private void tearDown(@NonNull PopupWindow dialog) {
        try {
            Thread.sleep(150);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            dialog.dismiss();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        networkCallback();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    public void networkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            EVENTS events = EVENTS.getInstance(getApplicationContext());
            ConnectivityManager.NetworkCallback networkCallback =
                    new ConnectivityManager.NetworkCallback() {
                        @Override
                        public void onAvailable(Network network) {
                            super.onAvailable(network);
                            events.online();
                        }

                        @Override
                        public void onLost(Network network) {
                            super.onLost(network);
                            events.offline();
                        }

                    };

            NetworkRequest networkRequest = new NetworkRequest.Builder()
                    .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                    .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                    .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                    .build();

            connectivityManager.registerNetworkCallback(networkRequest, networkCallback);


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void invalidateOptionsMenu() {
        super.invalidateOptionsMenu();
        browserFragment.invalidateOptionsMenu();
    }

    public enum WindowSizeClass {COMPACT, MEDIUM, EXPANDED}
}